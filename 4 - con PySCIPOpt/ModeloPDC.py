# *********************************************************************************************************************************
# * EJERCICIO en Clases - Patrones de corte *
# *********************************************************************************************************************************

# *********************************************************************************************************************************
# CONSIGNA
# Cuál es la mínima cantidad de rollos de $3$ metros de ancho que necesitamos fabricar para satisfacer la siguiente demanda?
# 97 rollos de 135 cm de ancho.
# 610 rollos de 108 cm de ancho.
# 395 rollos de 93 cm de ancho.
# 211 rollos de 42 cm de ancho.
# *********************************************************************************************************************************

from pyscipopt import Model, quicksum

class ModeloPDC:
    
    @staticmethod
    def resolverProblema(patters):
        
        # Modelo
        model = Model("TP : Patrones de corte")  

        # Input
        patrones=patters;
        cantidadPatrones = range(len(patrones));

        # Variables
        p = [model.addVar(vtype="INTEGER") for j in cantidadPatrones]

        # Funcion objetivo
        model.setObjective(sum(p), sense="minimize")

        # Restricciones
        model.addCons(quicksum(patron[1] * p[patron[0]] for patron in patrones) >= 97, "Rest. Rollo 1")
        model.addCons(quicksum(patron[2] * p[patron[0]] for patron in patrones) >= 610, "Rest. Rollo 2")
        model.addCons(quicksum(patron[3] * p[patron[0]] for patron in patrones) >= 395, "Rest. Rollo 3")
        model.addCons(quicksum(patron[4] * p[patron[0]] for patron in patrones) >= 211, "Rest. Rollo 4")

        # Optimizacion
        model.optimize()
        sol = model.getBestSol()

        # Mostrar solucion
        for i in cantidadPatrones:
            print( "p{}: {}".format(i, (sol[p[i]])) )

# *********************************************************************************************************************************
