
from ModeloPDC import *
from GeneradorPDC import *

# ****************************************************************************
# ***************************** Metodo Principal *****************************
# ****************************************************************************

# Generamos y obtenemos los patrones.
generador = GeneradorPDC();
patrones = generador.generarPatrones();
# Resolvemos el problema a traves del modelo de programacion lineal.
ModeloPDC.resolverProblema(patrones);

# ****************************************************************************
# ****************************************************************************