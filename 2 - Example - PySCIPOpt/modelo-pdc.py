# *********************************************************************************************************************************
# * EJERCICIO en Clases - Patrones de corte *
# *********************************************************************************************************************************

# *********************************************************************************************************************************
# CONSIGNA
# Cuál es la mínima cantidad de rollos de $3$ metros de ancho que necesitamos fabricar para satisfacer la siguiente demanda?
# 97 rollos de 135 cm de ancho. (w)
# 610 rollos de 108 cm de ancho. (x)
# 395 rollos de 93 cm de ancho. (y)
# 211 rollos de 42 cm de ancho. (z)
# *********************************************************************************************************************************
'''
# Lectura de inputs (patrones de corte)
set IWXYZ := { read "input.txt" as "<1n,2n,3n,4n,5n>" comment "#" };
set N := { 1 .. card(IWXYZ) };

# Declaracion de variables
var p[N] integer >= 0;

# Funcion objetivo
minimize cantidadDeRollos : sum <i> in N : p[i];

# Restricciones
subto restRolloW: ( sum <i,w,x,y,z> in IWXYZ :
                        w*p[i] ) >= 97;

subto restRolloX: ( sum <i,w,x,y,z> in IWXYZ :
                        x*p[i] ) >= 610; 

subto restRolloY: ( sum <i,w,x,y,z> in IWXYZ :
                        y*p[i] ) >= 395;

subto restRolloZ: ( sum <i,w,x,y,z> in IWXYZ :
                        z*p[i] ) >= 211;
'''
# *********************************************************************************************************************************

from pyscipopt import Model, quicksum

model = Model("Example")  

# Input
patrones=[[0,2,0,0,0],[1,1,1,0,1],[2,1,0,1,1],[3,1,0,0,3],[4,0,2,0,2],[5,0,1,2,0],[6,0,1,1,2],[7,0,1,0,4],[8,0,0,3,0],[9,0,0,2,2],[10,0,0,1,4],[11,0,0,0,7]];
cantidadPatrones = range(len(patrones));

# Variables
p = [model.addVar(vtype="INTEGER") for j in cantidadPatrones]

# Funcion objetivo
model.setObjective(sum(p), sense="minimize")

# Restricciones
model.addCons(quicksum(patron[1] * p[patron[0]] for patron in patrones) >= 97, "Rest. Rollo 1")
model.addCons(quicksum(patron[2] * p[patron[0]] for patron in patrones) >= 610, "Rest. Rollo 2")
model.addCons(quicksum(patron[3] * p[patron[0]] for patron in patrones) >= 395, "Rest. Rollo 3")
model.addCons(quicksum(patron[4] * p[patron[0]] for patron in patrones) >= 211, "Rest. Rollo 4")

# Optimizacion
model.optimize()
sol = model.getBestSol()

#Mostrat solucion
for i in cantidadPatrones:
    print( "p{}: {}".format(i, (sol[p[i]])) )

# *********************************************************************************************************************************
