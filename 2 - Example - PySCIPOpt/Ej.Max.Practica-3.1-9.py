# ***********************************
# * EJERCICIO de Practica -> 3.1-9. *
# ***********************************

# Variable definitions
# var x >= 0;
# var y >= 0;

# Objective function
# maximize obj: 5*x + 2*y;

# Constraints
# subto R1: 3*x + 2*y <= 2400;
# subto R2: 1*y <= 800;
# subto R3: 2*x <= 1200;

# ***********************************

from pyscipopt import Model

# model name is optional
model = Model("Example")  

# Variables
x = model.addVar("x", vtype="INTEGER")
y = model.addVar("y", vtype="INTEGER")

# Funcion objetivo
model.setObjective(5*x + 2*y, sense="maximize")

# Restricciones
model.addCons(3*x + 2*y <= 2400)
model.addCons(1*y <= 800)
model.addCons(2*x <= 1200)

# Optimizacion
model.optimize()
sol = model.getBestSol()

# Mostrar resultados
print("x: {}".format(sol[x]))
print("y: {}".format(sol[y]))

# ***********************************