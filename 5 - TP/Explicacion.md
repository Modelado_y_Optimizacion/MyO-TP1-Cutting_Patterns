
El trabajo paractico cuenta con 3 clases desarrolladas en python:

* 1º main.py : Programa principal que se encarga de llamar a GeneradorPDC y ModeloPDC
* 2º GeneradorPDC.py : Clase que genera y devuelve una serie de patrones (maximales) a partir de las caracteristicas del problema. En nuestro caso ancho de rollo y 'rollitos'.
* 3º ModeloPDC.py : Clase que se encarga de recibir los patrones y procesa la informacion con pyscipopt (libreria scip en python) y la muestra por pantalla.

main.py 
    - GeneradorPDC.py
        generarPatrones() : metodo principal de la clase a partir del cual llama a los otros metodos.
        ciclo:  
            - self.Cuentas(); : calcula a partir del resto/sobrante el nuevo patron.
                * resto(): metodo que devuelve el resto/material sobrante del patron anterior
                * crearPatron(): metodo que crea el proximo patron (maximal) a partir del resto.
            - self.PersistirPatron(); : se encarga de guardar el patron en una lista que al final del proceso devolvera.
            - self.FinalizarCiclo(); : Evalua si a traves de los puntero (estatico y dinamico) si se termino de generar todos los patrones y retorna un booleano.
            - self.MoverPunteros(); : Mueve los punteros (estatico y dinamico) para evaluar y crear el proximo patron.
                * limpiarArrayPatronDesde(): metodo que se encarga de limpiar el patron anterior dependiendo de los punteros.
                * maximizarPunteroDinamicoDesde(): metodo que se encarga de maximizar el valor sobre el puntero dinamico. Es la base sobre el cual ir decrementando patron[punteroDinamico].
                * maximizarPunteroEstaticoDesde(): metodo que se encarga de maximizar el valor sobre el puntero estatico. Es la base sobre el cual ir decrementando patron[punteroEstatico].

    - ModeloPDC.py
        * Crea el modelo.
        * Toma el input (patrones) recibido de GeneradorPDC.py.
        * Crea las variables del problema.
        * Define la funcion objetivo del problema.
        * Define las restricciones del problema.
        * Optimiza y obtiene la solucion del problema.
        * Muestra por pantalla la solucion.
