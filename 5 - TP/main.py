
from ModeloPDC import *
from GeneradorPDC import *

# ****************************************************************************
# ***************************** Metodo Principal *****************************
# ****************************************************************************

# Declaracion de inputs.
anchoRollo = 0;
cantidad = [];
ancho = [];

# Lectura de inputs.
f = open('input.txt','r')
linea=f.readline()
contador=1;
while linea!='':
    if(contador==2):
        anchoRollo = int(linea[0:len(linea)-1]);
    if(contador>=4):
        indice_separator = linea.index(':');
        cant = linea[0:indice_separator];
        anch =  linea[indice_separator+1:len(linea)-1];
        cantidad.append(int(cant));
        ancho.append(int(anch));
    linea=f.readline();
    contador=contador+1;
f.close()

# Ordenamos los inputs (cantidad:ancho) en base a ancho.
tupla = list(zip(ancho, cantidad));
tuplaOrdenada = sorted(tupla, reverse=True);
print(tuplaOrdenada);
ancho, cantidad = zip(*tuplaOrdenada)

# Generamos y obtenemos los patrones.
generador = GeneradorPDC(ancho, anchoRollo);
patrones = generador.generarPatrones();
# Resolvemos el problema a traves del modelo de programacion lineal.
ModeloPDC.resolverProblema(patrones, cantidad);

# ****************************************************************************
# ****************************************************************************